import Models.Gestor;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Testes {

    @Test
    public void readJsonTest_1() {
        Gestor gestor = new Gestor();

        assertTrue(gestor.readJson("files/mapa.json"));

    }

    @Test
    public void readJsonTest_2() {
        Gestor gestor = new Gestor();

        assertFalse(gestor.readJson("files/erro_X.json"));

    }
}