package GUI;

import Estruturas.Network;
import Exececoes.EmptyException;
import Interfaces.JanelaSimulacaoADT;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;

/**
 * Classe que permite jogar o modo simulação
 */
public class JanelaSimulacao extends JFrame implements JanelaSimulacaoADT {

    private Network<String> network;

    public JanelaSimulacao(Network<String> network) {
        super();
        this.network = network;
        definirConteudo();
    }

    /**
     * Define o tamanho, localizacao e layout da janela
     */
    @Override
    public void definirConteudo() {
        getContentPane().setLayout(null); //atribui um layout nulo a janela
        setBounds(650, 50, 650, 550); //define o tamanho e posiçao da janela
        setTitle("Janela Simulacao"); //define titulo da janela

        JLabel jLabel_caminho = new JLabel();
        jLabel_caminho.setText(getMelhorCaminho());
        jLabel_caminho.setBounds(0, 0, getWidth(), 0); //define posicao e tamanho da label
        jLabel_caminho.setBackground(Color.CYAN);

        JLabel jLabel_tabela = new JLabel();
        jLabel_tabela.setText(getTabelaGrafo());
        jLabel_tabela.setBounds(0, 0, getWidth(), 0); //define posicao e tamanho da label
        jLabel_tabela.setBackground(Color.RED);

        JLabel jLabel_adjacencias = new JLabel();
        jLabel_adjacencias.setText(getAdjacencias());
        jLabel_adjacencias.setBounds(0, 0, getWidth(), 0); //define posicao e tamanho da label
        jLabel_adjacencias.setBackground(Color.GREEN);

        JPanel container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBounds(0, 0, getWidth(), getHeight());

        container.add(jLabel_caminho);
        container.add(jLabel_tabela);
        container.add(jLabel_adjacencias);

        JScrollPane scrPane = new JScrollPane(container);
        scrPane.setBounds(0, 0, getWidth(), getHeight());
        setContentPane(scrPane);

        //listener (evento) pra quando fecha a janela
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                dispose();
            }
        });

        setVisible(true);
    }

    /**
     * Metodo de devolve uma string para ser mostrada numa janela com html de forma a imprimir uma tabela com a matriz de ligacoes da network
     *
     * @return string da matriz em forma de tabale usando html
     */
    @Override
    public String getTabelaGrafo() {
        StringBuilder tabela = new StringBuilder("" +
                "<html>" +
                "   <head>" +
                "       <style>" +
                "           table {font-family: arial, sans-serif; border-collapse: collapse; width: 100%;}" +
                "           td, th {border: 1px solid; text-align: center; padding: 4px;}" +
                "       </style>" +
                "   </head>" +
                "<body>" +
                "   <table>" +
                "       <tr>");
        int i;
        for (i = 0; i < network.getwAdjMatrix().length + 1; ++i) {
            if (i != 0) {
                tabela.append("<th>").append(network.getVertex(i - 1)).append("</th>");
            } else {
                tabela.append("<th>" + "</th>");
            }
        }

        tabela.append("</tr>");

        for (i = 0; i < network.getwAdjMatrix().length; ++i) {
            tabela.append("<tr>").append(network.getVertex(i));

            for (int j = 0; j < network.getwAdjMatrix()[i].length; ++j) {
                if (network.getwAdjMatrix()[i][j] == Double.POSITIVE_INFINITY) {
                    tabela.append("<td color=\"orange\">\\\\</td>");
                } else {
                    tabela.append("<td color=\"blue\">").append((int) network.getwAdjMatrix()[i][j]).append("</td>");
                }
            }

            tabela.append("</tr>");
        }
        tabela.append("</table></body></html>");
        return tabela.toString();
    }

    /**
     * Método que devolve um iterador com o caminho mais curto e com menos dano
     *
     * @return iterador com o caminho mais curto e com menos dano
     * @throws EmptyException se estiver vazio
     */
    @Override
    public Iterator getMelhorCaminhoIterator() throws EmptyException {
        return network.iteratorShortestPathIndices(network.getIndex("entrada"), network.getIndex("exterior")); //obter o melhor caminho
    }

    /**
     * Método permite encontrar o caminho mais curto e com menos dano
     *
     * @return uma string com o caminho mais curto e com menos dano
     */
    @Override
    public String getMelhorCaminho() {
        StringBuilder trajeto = new StringBuilder("<html><p style=\"text-align:center;font-size:16px\">entrada"); //caminho começa na estrada

        try {
            Iterator iterator = getMelhorCaminhoIterator(); //obter o melhor caminho
            iterator.next(); //passar a entrada a frente
            while (iterator.hasNext()) { //percorrer o trajeto mais curto
                trajeto.append(" -> ").append(network.getVertex((Integer) iterator.next())); //adicionar aposento ao trajeto
            }
        } catch (Exception e) {
            System.out.println("Erro no metodo iteratorShortestPath()");
        }
        trajeto.append("</p></html>");
        return trajeto.toString();
    }

    /**
     * Método que devolve os pesos e as ligacoes adjacentes
     *
     * @return uma string com os os pesos e as ligacoes adjacentes
     */
    @Override
    public String getAdjacencias() {
        StringBuilder stringGrafo = new StringBuilder("<html>");

        for (int i = 0; i < network.getwAdjMatrix().length; i++) {
            stringGrafo.append("<p color=\"blue\">").append(network.getVertex(i));
            for (int j = 0; j < network.getwAdjMatrix()[i].length; j++) {
                if (network.getwAdjMatrix()[i][j] != Double.POSITIVE_INFINITY)
                    stringGrafo.append("<b color=\"red\"> -> <b color=\"black\">(").append(network.getVertex(j)).append(", ").append((int) network.getwAdjMatrix()[i][j]).append(")");
            }
            stringGrafo.append("</p>");
        }
        stringGrafo.append("</html>");
        return stringGrafo.toString();
    }

}
