package GUI;

import Exececoes.EmptyException;
import Ficha_12.Ex_4.ArrayUnorderedList.ArrayUnorderedList;
import Interfaces.JanelaJogoADT;
import Models.Gestor;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;

/**
 * Classe que permite criar uma JFrame com os botões necessários para a jogabilidade
 */
public class JanelaJogo extends JFrame implements JanelaJogoADT {

    private String modo;
    private Gestor gestor;
    private JLabel jLabel_nome;
    private String nome_mapa;
    private String nome_jogador;
    private int dificuldade;
    private int num_movimentos;
    private double fator = 1;
    private JanelaSimulacao janelaSimulacao = null;
    private Iterator iterator_caminho;

    /**
     * Construtor
     *
     * @param nome_jogador nome do jogador
     * @param nome_mapa nome do ficheiro json do mapa
     * @param dificuldade nivel de dificuldade
     * @param modo modo de jogo "manual" ou "simulacao"
     */
    public JanelaJogo(String nome_jogador, String nome_mapa, int dificuldade, String modo) {
        super();
        this.nome_mapa = nome_mapa;
        this.nome_jogador = nome_jogador;
        this.dificuldade = dificuldade;
        this.modo = modo;
        this.iterator_caminho = null;
        definirConteudo();
    }

    /**
     * Define o tamanho, localizacao e layout da janela
     */
    public void definirConteudo() {
        this.jLabel_nome = new JLabel();
        this.gestor = new Gestor();
        this.gestor.startMusic();
        this.num_movimentos = 0;

        gestor.readJson("files/" + nome_mapa); //le o json

        getContentPane().setLayout(null); //atribui um layout nulo a janela
        setBounds(0, 50, 640, 380); //define o tamanho e posiçao da janela
        setResizable(false); //impede a alteracao do tamanho da janela
        setTitle("Casa Assombrada - " + gestor.getMapa().getNome()); //define titulo da janela

        if (modo.equals("simulacao")) {
            janelaSimulacao = new JanelaSimulacao(gestor.getNetwork()); //abre a janela com o caminho otimo, matriz e a lista de adjacencias
            try {
                this.iterator_caminho = janelaSimulacao.getMelhorCaminhoIterator();
                iterator_caminho.next();
            } catch (EmptyException e) {
                e.printStackTrace();
            }
        }

        setImage("files/entrada.jpg", "entrada"); //define imagem inicial e nome da label
        setBotoes(gestor.findLigacoes("entrada")); //define os botoes de escolhas quando o jogador esta na entrada da casa
        fator = gestor.getMapa().getPontos() / 100.0; //fator que ajuda a calcular os pontos na barra de vida
        mostrarBarraPontos(); //mostra a barra de pontos
        mostrarBotaoDesistir(); //cria o botao de desistir

        //listener pra quando fecha a janela
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                if (modo.equals("simulacao"))
                    janelaSimulacao.dispose();
                dispose();
            }
        });


        setVisible(true);
    }

    /**
     * Define a imagem de fundo da janela e o nome
     *
     * @param file_path caminho pra a imagem
     * @param nome      nome do aposento
     */
    public void setImage(String file_path, String nome) {
        getContentPane().removeAll(); //remove todos os elementos da janela
        repaint(); //repinta a janela
        JLabel jLabel = new JLabel(new ImageIcon(file_path)); //carrega imagem
        setContentPane(jLabel); //adiciona imagem ao fundo da janela

        jLabel_nome.setText(nome); //define o texto da label com o nome do aposento onde se encontra
        jLabel_nome.setOpaque(true); //label opaca
        jLabel_nome.setBounds(0, 0, getWidth(), 20); //define posicao e tamanho da label
        jLabel_nome.setBackground(Color.RED); //cor de fundo da label
        add(jLabel_nome); //adiciona label a janela
        setVisible(true); //atualiza a janela
    }

    /**
     * Metodo que mostra um botao para o jogador poder desisitir
     */
    public void mostrarBotaoDesistir() {
        if (!modo.equals("simulacao")) { //nao mostra o botao de desitir no modo simulacao
            JButton button = new JButton("Desistir"); //cria um botao com o texto "Desistir"
            button.setBounds(520, 310, 100, 25); //define posicao e tamanho do botao

            //evento para quando o utilizador clicar no botao de desistir
            button.addActionListener(e -> {
                dispose(); //fecha a janela
                JOptionPane.showMessageDialog(null, "Desistiu!!!", "Desistiu", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("files/gastly_icon.png")); //mensagem que indica que o utilizador desistiu
            });

            add(button); //adicionar botao a janela de escolhas
        }
    }

    /**
     * Metodo que mostra a imagem do fantasma quando o aposento tem um fantasma
     */
    public void mostrarFantasma() {
        JLabel panel_ghost = new JLabel(new ImageIcon("files/ghost.png")); //criar label com a imagem do fantasma
        panel_ghost.setBounds(0, 0, getWidth(), getHeight()); //limites e posicao da labelcom a imagem do fantasma
        add(panel_ghost); //adicionar label a janela
    }

    /**
     * Metodo que mostra uma barra de progresso com os pontos do utilizador
     */
    public void mostrarBarraPontos() {
        JProgressBar progressBar_pontos = new JProgressBar(); //cria uma barra de progresso
        progressBar_pontos.setBounds(10, getHeight() - 60, 250, 15); //definir limites e posicao da barra
        progressBar_pontos.setForeground(Color.ORANGE); //define cor da barra
        progressBar_pontos.setValue((int) Math.ceil(gestor.getPontos_jogador() / fator)); //define a vida na barra
        add(progressBar_pontos); //adiciona barra de progresso a janela
        System.out.println("Vida: " + gestor.getPontos_jogador() + "/" + gestor.getMapa().getPontos()); //mostra mensagem que o aposento contem um fantasma
    }

    /**
     * Metodo que cria os botos com as opcoes que o utilizador tem para escolher
     * Cada botao representa um aposento para onde o jogador pode ir
     *
     * @param aposentos array de aposentos
     */
    public void setBotoes(ArrayUnorderedList<String> aposentos) {
        JButton button; //cria um botao

        JPanel jPanel_botoes = new JPanel(); //jPanel dos botoes com as ligacoes ao aposento
        jPanel_botoes.setLayout(new FlowLayout()); //layout do jPanel
        jPanel_botoes.setBounds(0, 250, getWidth(), 100); //define posicao e tamanho do jPanel
        jPanel_botoes.setOpaque(false); //retira fundo do jPanel
        //limpa o conteudo do jPanel, caso contenha botoes do aposento anterior
        jPanel_botoes.removeAll();
        jPanel_botoes.repaint();

        add(jPanel_botoes); //adicionar jPanel a janela

        mostrarBotaoDesistir(); //mostra o botao de desistir

        String proximo_aposento = "";

        if (modo.equals("simulacao") && iterator_caminho.hasNext()) {
            proximo_aposento = (String) gestor.getNetwork().getVertex((Integer) iterator_caminho.next());
        }

        //percorre array de aposentos recebido e cria um botao para cada aposento
        for (String aposento : aposentos) {
            button = new JButton(aposento); //cria um botao com o nome do aposento
            if (aposento.equals("exterior")) { //verifica se o aposento é pra o exterior
                button.setForeground(Color.RED); //define cor das letras
            }

            if (modo.equals("simulacao") && !proximo_aposento.equals(aposento)) {
                button.setEnabled(false); //impede o utilizador de clicar noutro botao que nao leve a saida mais rapido
            }

            //adiciona um listener para quando o utilizador clicar no botao do aposento escolhido
            button.addActionListener(e -> {
                setImage("files/" + e.getActionCommand() + ".jpg", e.getActionCommand()); //altera a imagem da janela e o nome da label

                num_movimentos++; //adiciona 1 movimento ao numero de movimentos

                //dano do fantasma tendo em conta a difuldade
                int fantasma_dano = gestor.checkGhost(e.getActionCommand()) * dificuldade;
                gestor.removerPontos(fantasma_dano); //remove pontos

                //verifica se o jogador ainda tem pontos
                if (gestor.getPontos_jogador() > 0) {
                    //caso tenha conseguido escapar
                    if (aposento.equals("exterior")) {
                        gestor.adicionarClassificacao(nome_jogador, nome_mapa, dificuldade, num_movimentos); //adiciona classificacao
                        dispose(); //fecha a janela do jogo e volta ao menu
                        JOptionPane.showMessageDialog(null, "Escapou da Casa Assombrada", "Escapou", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("files/gastly_icon.png"));
                    } else {
                        setBotoes(gestor.findLigacoes(e.getActionCommand())); //mostra os botoes das ligacoes ao aposento
                    }
                } else {
                    gestor.adicionarClassificacao(nome_jogador, nome_mapa, dificuldade, num_movimentos); //adiciona classificacao
                    dispose(); //fecha a janela do jogo e volta ao menu
                    JOptionPane.showMessageDialog(null, "Perdeu o jogo", "Perdeu", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("files/gastly_icon.png"));
                }

                //caso o aposento tenha fantasma
                if (fantasma_dano > 0) {
                    mostrarFantasma(); //mostra fantasma na janela do jogo
                }
                mostrarBarraPontos(); //mostra barra de pontos atualizada
            });

            if (!aposento.equals("entrada")) { //se o botao for para voltar pra entrada nao mostra essa opcao
                jPanel_botoes.add(button); //adiciona botao a janela
            }
            setVisible(true); //atualiza a janela
        }
    }

    @Override
    public void dispose() {
        gestor.getClip().stop(); //para a musica ao sair
        if (modo.equals("simulacao"))
            janelaSimulacao.dispose();
        new JanelaMenu(); //abre o menu quando fecha a janela
        super.dispose();
    }
}
