package GUI;

import Estruturas.ArrayOrderedList;
import Interfaces.JanelaClassificacoesADT;
import Models.Gestor;
import Models.Utilizador;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Objects;

/**
 * Classe que permite mostrar a janela com as classificaões
 */

public class JanelaClassificacoes extends JFrame implements JanelaClassificacoesADT {

    private Gestor jogarModoManual = new Gestor();
    private ArrayOrderedList<Utilizador> classificacoes;
    private String nome_mapa; //nome do jogador e do mapa escolhido
    private JComboBox<String> comboBox;

    public JanelaClassificacoes(String nome_mapa) {
        super("Casa Assombrada - Classificacoes"); //define titulo da janela
        this.nome_mapa = nome_mapa;
        adicionarConteudo(); //adiciona o conteudo a janela
    }

    /**
     * Método que permite definir as definiçoes gerais da janela Simulaçao
     */
    @Override
    public void adicionarConteudo() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //programa termina ao fechar a janela
        setBounds(600, 50, 500, 500); //define tamanho e posiçao da janela
        setIconImage((new ImageIcon("files/gastly_icon.png")).getImage()); //definir icon da janela

        //imprimir classificacoes numa janela
        imprimeClassificacoes();
    }

    public void imprimeClassificacoes() {
        getContentPane().removeAll();
        repaint();
        jogarModoManual.carregaClassificacoes(); //carregar classificacoes do ficheiro
        classificacoes = jogarModoManual.getClassificacoes(); //array de classificacoes

        StringBuilder stringClassificacoes = new StringBuilder("" +
                "<html>" +
                "<head>" +
                "<style>" +
                "table {font-family: arial, sans-serif; border-collapse: collapse; width: 100%;}" +
                "td, th {border: 1px solid; text-align: left; padding: 4px;}" +
                "</style>" +
                "</head>" +
                "<body>" +
                "<h1>Classificacoes</h1>" +
                "<table>");

        //mostrar classificacoes do mapa por ordem de pontos e tempo
        stringClassificacoes.append("<tr>");
        stringClassificacoes.append("<th>").append("Nome").append("</th>").append("<th>").append("Pontos").append("</th>").append("<th>").append("Tempo").append("</th>").append("<th>").append("Movimentos").append("</th>").append("<th>").append("Dificuladade").append("</th>").append("<th>").append("Data").append("</th>"); //header da tabela
        stringClassificacoes.append("</tr>");

        for (Utilizador classificacao : classificacoes) {
            if (classificacao.getMapa().equals(nome_mapa)) { //verifica se a classificacao pertence ao mapa
                stringClassificacoes.append("<tr><td>").append(classificacao.getNome()).append("</td>").append("<td>").append(classificacao.getPontos()).append("</td>").append("<td>").append(classificacao.getTempo()).append("</td>").append("<td>").append(classificacao.getMovimentos()).append("</td>").append("<td>").append(classificacao.getDificuldade()).append("</td>").append("<td>").append(new SimpleDateFormat("HH:mm:ss dd-MM-yyyy").format(classificacao.getData())).append("</td>"); //adiciona pontuacao a string
            }
        }

        stringClassificacoes.append("</table><br/></body></html>");

        JLabel label_classificacoes = new JLabel();
        label_classificacoes.setText(stringClassificacoes.toString());


        JPanel container = new JPanel();
        container.add(label_classificacoes);

        JScrollPane scrPane = new JScrollPane(container);
        scrPane.setBounds(0, 0, getWidth(), getHeight());
        add(scrPane);

        setVisible(true);
    }
}
