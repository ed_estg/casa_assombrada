package GUI;

import Interfaces.JanelaMenuADT;

import javax.swing.*;
import java.io.File;
import java.util.Objects;

/**
 * Classe que permite criar uma JFrame com todas os botões onde permite meter o nome do utilizador,
 * escolher o mapa, escolher a dificuldade, ver regras de jogo, ver classificaçóes e sair do programa
 */
public class JanelaMenu extends JFrame implements JanelaMenuADT {

    private String nome_mapa;
    private JTextField textField_nome;
    private int dificuldade;

    public JanelaMenu() {
        super("Casa Assombrada - Menu"); //define titulo da janela
        this.dificuldade = 1; //define a dificuldade como 1 (basico)
        adicionarConteudo(); //adiciona o conteudo a janela
    }

    /**
     * Metodo que adiciona o conteudo a janela
     */
    public void adicionarConteudo() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //programa termina ao fechar a janela
        getContentPane().setLayout(null); //atribui um layout nulo a janela
        setBounds(0, 50, 400, 380); //define tamanho e posiçao da janela
        setResizable(false); //impede a alteracao do tamanho da janela
        setIconImage((new ImageIcon("files/gastly_icon.png")).getImage()); //definir icon da janela

        readNomeJogador(); //mostra a textField para introduzir o nome do jogador
        escolherMapa(); //mostra a comboBox para escolher o mapa
        botoesDificuldade(); //mostrar botoes para escolher a dificuldade
        botaoModoManual(); //mostra o botao para começar o modo manual
        botaoModoSimulacao(); //mostra o botao para começar o modo simulacao
        botaoRegras(); //mostra um botao que mostra as regras do jogo
        botaoClassificacoes(); //mostra botao para mostrar as classificacoes para o mapa escolhido
        botaoSair(); //botao para fechar a janela e terminar o programa

        setVisible(true); //torna a janela visivel
    }

    /**
     * Metodo que mostra uma label e uma textField para introduzir o nome do Utilizador
     */
    public void readNomeJogador() {
        JLabel jLabel_nome = new JLabel("Nome Jogador:"); //cria label com o texto "Nome Jogador:"
        jLabel_nome.setBounds(20, 10, 90, 25); //define tamanho e posiçao da label
        add(jLabel_nome); //adiciona label a janela

        textField_nome = new JTextField("player"); //criar textField com o texto "player"
        textField_nome.setBounds(120, 10, 100, 25); //define tamanho e posiçao da textField
        add(textField_nome); //adiciona textField a janela
    }

    /**
     * Metodo procura ficheiros do tipo json na pasta files
     *
     * @return array do tipo String com os nomes dos ficheiros do tipo json
     */
    private String[] getArrayFilesJSON() {
        File folder = new File("files/"); //pasta onde procurar os ficheiros do tipo json
        String[] mapas_files = new String[0]; //array com o nome dos ficheiros do tipo json
        for (File file : Objects.requireNonNull(folder.listFiles())) { //percorre todos os ficheiros da pasta
            if (file.getName().indexOf(".json") > 0) { //verifica se o ficheiro é do tipo JSON
                String[] temp = new String[mapas_files.length + 1];
                System.arraycopy(mapas_files, 0, temp, 0, mapas_files.length);
                temp[mapas_files.length] = file.getName(); //adicona nome do ficheiro ao array
                mapas_files = temp;
            }
        }
        return mapas_files;
    }

    /**
     * Metodo que mostra uma combobox para escolher o mapa
     * Procura pelos ficheiros do tipo json na pasta files
     */
    public void escolherMapa() {
        JLabel jLabel_mapa = new JLabel("Mapa:"); //cria label com o texto "Mapa:"
        jLabel_mapa.setBounds(20, 50, 75, 25); //define tamanho e posiçao da label
        add(jLabel_mapa); //adiciona label a janela

        JComboBox<String> comboBox = new JComboBox<>(getArrayFilesJSON()); //comboBox com as opcoes de mapas
        comboBox.setBounds(120, 50, 150, 30); //define tamanho e posiçao da comboBox

        //verifica se existem mapas e atribui a variavel o nome do primeiro ficheiro do tipo json
        if (comboBox.getItemCount() > 0)
            nome_mapa = comboBox.getItemAt(0);

        //evento ao escolher um item da comboBox
        comboBox.addActionListener(e -> {
            nome_mapa = (String) comboBox.getSelectedItem(); //define o nome do mapa
        });
        add(comboBox); //adiciona comboBox a janela
    }

    /**
     * Metodo que mostra o botao para começar o modo Manual
     */
    public void botaoModoManual() {
        JButton button_start = new JButton("Start Manual"); //cria um botao com o texto "Start Manual"
        button_start.setBounds(20, 130, 120, 25); //define limites e posicao do botao
        getContentPane().add(button_start); //adiciona botao a janela

        //evento ao clicar no botao
        button_start.addActionListener(actionEvent -> {
            //verifica se o nome do mapa nao esta vazio
            if (!nome_mapa.isEmpty()) {
                //verifica se o textField do nome do mapa nao esta vazio e se o nome do jogador nao esta vezio ou contem carateres invalidos ou esta fora dos limites de carateres (entre 3 e 10)
                if (!textField_nome.getText().trim().isEmpty() && !textField_nome.getText().contains(";") && textField_nome.getText().length() >= 3 && textField_nome.getText().length() <= 10) {
                    new JanelaJogo(textField_nome.getText(), nome_mapa, dificuldade, "manual"); //abre a janela do jogo e começa o jogo manual
                    dispose(); //fecha a janela
                } else {
                    JOptionPane.showMessageDialog(null, "Nao pode estar vazio.\nNao pode conter caracteres invalidos.\nTem de ter entre 3 e 10 caracteres.", "Nome do Jogador", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("files/gastly_icon.png"));
                }
            } else {
                JOptionPane.showMessageDialog(null, "Pasta files nao contem nenhum mapa que posso ser carregado.", "Mapas de jogo", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("files/gastly_icon.png"));
            }
        });
    }

    /**
     * Metodo que mostra o botao para começar o modo Simulacao
     */
    public void botaoModoSimulacao() {
        JButton button_simulacao = new JButton("Start Simulacao"); //cria um botao com o texto "Start Simulacao"
        button_simulacao.setBounds(150, 130, 130, 25); //define os limites e posicao do botao
        getContentPane().add(button_simulacao); //adiciona botao a janela

        //evento ao clicar no botao
        button_simulacao.addActionListener(actionEvent -> {
            //verifica se o nome do mapa nao esta vazio
            if (!nome_mapa.isEmpty()) {
                new JanelaJogo(textField_nome.getText(), nome_mapa, dificuldade, "simulacao"); //abre a janela de jogo e começa a simulacao
                dispose(); //fecha a janela
            } else {
                JOptionPane.showMessageDialog(null, "Pasta files nao contem nenhum mapa que posso ser carregado.", "Mapas de jogo", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("files/gastly_icon.png"));
            }
        });
    }

    /**
     * Metodo para mostrar as opcoes de dificuldade
     * Opçoes: Basico, Normal, Dificil
     */
    public void botoesDificuldade() {
        JLabel jLabel_dificuldade = new JLabel("Dificuldade:"); //cria uma label com o texto "Dificuldade:"
        jLabel_dificuldade.setBounds(20, 90, 75, 25); //define limites e posicao da label
        add(jLabel_dificuldade); //adiciona label a janela

        //botoes com os modos de jogo (basico, normal, dificil)
        JRadioButton radioButton_basico = new JRadioButton("Basico"); //radioButton para modo Basico
        JRadioButton radioButton_normal = new JRadioButton("Normal"); //radioButton para modo Normal
        JRadioButton radioButton_dificil = new JRadioButton("Dificil"); //radioButton para modo Dificil
        //define limites e posicao dos botoes
        radioButton_basico.setBounds(100, 90, 70, 25);
        radioButton_normal.setBounds(180, 90, 70, 25);
        radioButton_dificil.setBounds(260, 90, 70, 25);
        ButtonGroup bg = new ButtonGroup(); //cria grupo de botoes
        //adicionar botoes a janela
        add(radioButton_basico);
        add(radioButton_normal);
        add(radioButton_dificil);
        //adicionar botoes ao grupo de botoes
        bg.add(radioButton_basico);
        bg.add(radioButton_normal);
        bg.add(radioButton_dificil);
        radioButton_basico.setSelected(true); //selecionar o botao do nivel Basico
        //evento ao clicar nos botoes, muda a dificuldade do jogo
        radioButton_basico.addActionListener(actionEvent -> dificuldade = 1);
        radioButton_normal.addActionListener(actionEvent -> dificuldade = 2);
        radioButton_dificil.addActionListener(actionEvent -> dificuldade = 3);
    }

    /**
     * Metodo com um botao que mostra as regras do jogo
     */
    public void botaoRegras() {
        JButton button_tutorial = new JButton("Regras do Jogo"); //cria um botao com o texto "Regras do jogo"
        button_tutorial.setBounds(20, 170, 150, 25); //define tamanho e posicao do botao
        getContentPane().add(button_tutorial); //adiciona botao a janela

        //evento ao clicar no botao
        button_tutorial.addActionListener(actionEvent -> JOptionPane.showMessageDialog(null,
                "O objetivo do jogo esta em encontrar o exterior.\n" +
                        "Sempre que entra numa sala com um fantasma sao retirados pontos de vida.\n" +
                        "Quando os pontos de vida chegarem a zero, o jogo acaba em derrota.\n" +
                        "O nivel de dificuldade altera o dano provocado pelos fantasmas.\n" +
                        "O jogador pode alterar o nome e escolher o mapa.\n" +
                        "As classificacoes mostram os resultados do mapa selecionado, ordenados por pontos e por tempo de jogo.\n" +
                        "Apos entrar na casa, nao pode voltar atras e as unicas maneiras de sair sao: encontrando o exterior, morrendo ou desisitindo.",
                "Regras do Jogo", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("files/gastly_icon.png")));
    }

    /**
     * Metodo que cria um botao para ver as classificaçoes do jogadores
     */
    public void botaoClassificacoes() {
        JButton button_classificacoes = new JButton("Classificacoes"); //cria um botao com o texto "Classificacoes"
        button_classificacoes.setBounds(20, 210, 200, 25); //define tamanho e posicao do botao
        getContentPane().add(button_classificacoes); //adiciona botao a janela

        //evento ao clicar no botao
        button_classificacoes.addActionListener(actionEvent -> {
            System.out.println("Mostrar Classificacoes");
            //mostrar as classificacoes
            new JanelaClassificacoes(nome_mapa);
        });
    }

    /**
     * Metodo que cria um botao pra sair do programa
     */
    public void botaoSair() {
        JButton button_sair = new JButton("Sair"); //cria um botao com o texto "Sair"
        button_sair.setBounds(20, 250, 100, 25); //define tamanho e posicao do botao
        getContentPane().add(button_sair); //adiciona botao a janela

        //evento ao clicar no botao
        button_sair.addActionListener(actionEvent -> {
            System.out.println("Adeus!");
            dispose(); //fecha a janela e termina o programa
        });
    }

}
