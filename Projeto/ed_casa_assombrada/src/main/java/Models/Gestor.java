package Models;

import Estruturas.ArrayOrderedList;
import Estruturas.Network;
import Ficha_12.Ex_4.ArrayUnorderedList.ArrayUnorderedList;
import Interfaces.GestorADT;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.sound.sampled.*;
import javax.swing.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * Classe que permite criar um gestor
 */
public class Gestor implements GestorADT {

    private Mapa mapa; //dados do mapa
    private Network<String> network; //network
    private ArrayOrderedList<Utilizador> classificacoes;
    private long start_game; //tempo em que o jogo comeca
    private int pontos_jogador; //pontos do jogador
    private Clip clip; //musica

    /**
     * Contrutor
     */
    public Gestor() {
        carregaClassificacoes(); //carrega as classificacoes guardadas
        this.start_game = Calendar.getInstance().getTimeInMillis(); //guarda o momento em que o jogador começa o jogo
        this.pontos_jogador = 0;
    }

    public Mapa getMapa() {
        return mapa;
    }

    public int getPontos_jogador() {
        return pontos_jogador;
    }

    public Clip getClip() {
        return clip;
    }

    public Network getNetwork() {
        return network;
    }

    public ArrayOrderedList<Utilizador> getClassificacoes() {
        return classificacoes;
    }

    /**
     * Metodo para adicionar classificacao ao array e guardar no ficheiro
     *
     * @param nome_jogador   nome do jogador
     * @param nome_mapa      nome do mapa
     * @param dificuladade   nivel de dificuldade jogado
     * @param num_movimentos numero de movimentos feitos
     */
    @Override
    public void adicionarClassificacao(String nome_jogador, String nome_mapa, int dificuladade, int num_movimentos) {
        carregaClassificacoes(); //carrega todas classificacoes guardadas
        long end_game = Calendar.getInstance().getTimeInMillis(); //regista tempo quando o jogo acaba

        classificacoes.add(new Utilizador(nome_jogador, pontos_jogador, nome_mapa, new Date(), (end_game - start_game) / 1000.0, dificuladade, num_movimentos));
        guardarClassificacoes(); //guardas todas as classificacoes no ficheiro
    }

    /**
     * Le o ficheiro JSON e inicializa o mapa e com os dados do ficheiro JSON
     *
     * @param file_path - caminho do ficheiro a ler
     * @return sucesso ou insucesso da operacao
     */
    @Override
    public boolean readJson(String file_path) {
        JSONParser parser = new JSONParser();
        try (Reader reader = new FileReader(file_path)) { //tenta ler o ficheiro json recebido
            JSONObject jsonObject = (JSONObject) parser.parse(reader);

            String nome = jsonObject.get("nome").toString(); //nome do mapa
            int pontos = Integer.parseInt(jsonObject.get("pontos").toString()); //pontos do mapa
            ArrayUnorderedList<Aposento> aposentos = new ArrayUnorderedList<>(); //lista de aposentos

            JSONArray jsonArray_mapa = (JSONArray) jsonObject.get("mapa");

            for (Object obj : jsonArray_mapa) { //percorre array mapa
                JSONObject it = (JSONObject) obj;
                String nome_aposento = it.get("aposento").toString(); //nome do aposento
                int fantasma = Integer.parseInt(it.get("fantasma").toString()); //pontos do fantasma no aposento
                ArrayUnorderedList<String> ligacoes_aposento = new ArrayUnorderedList<>(); //lista de ligacoes ao aposento

                JSONArray jsonArray_ligacoes = (JSONArray) it.get("ligacoes");
                for (Object jsonArray_ligacao : jsonArray_ligacoes) { //percorre array ligacoes
                    String ligacao = jsonArray_ligacao.toString();
                    ligacoes_aposento.addToFront(ligacao); //adiciona aposento a lista de ligacoes do aposento
                }
                //adiciona aposento a lista de aposentos
                aposentos.addToFront(new Aposento(nome_aposento, fantasma, ligacoes_aposento));
            }
            this.mapa = new Mapa(nome, pontos, aposentos); //cria um mapa com os dados do ficheiro JSON
            this.pontos_jogador = pontos; //pontos iniciais do jogador
            criarNetwork(); //cria a network
            return true;
        } catch (FileNotFoundException e) { //ficheiro nao existe
            JOptionPane.showMessageDialog(null, "Erro mapa nao encontrado!!!");
            System.out.println("Erro no metodo readJson() - file not found");
        } catch (Exception e) { //ficheiro contem erros
            JOptionPane.showMessageDialog(null, "Erro na leitura do mapa!!!");
            System.out.println("Erro no metodo readJson()");
        }
        return false;
    }

    /**
     * Cria a network do mapa
     * Cria as vertices, arestas, matriz
     */
    public void criarNetwork() {
        network = new Network<>();

        //cria os vertices
        network.addVertex("entrada"); //vertice da entrada
        for (Aposento aposento : mapa.getAposentos()) {
            network.addVertex(aposento.getNome()); //vertices dos aposentos
        }
        network.addVertex("exterior"); //vertice do exterior

        //percorrer as ligacoes dos aposentos e adicionar como arestas ao grafo
        for (Aposento obj_aposento : mapa.getAposentos()) {
            for (String obj_ligacao : obj_aposento.getLigacoes()) {
                if (!obj_ligacao.equals("exterior"))
                    network.addUniEdge(obj_ligacao, obj_aposento.getNome(), obj_aposento.getFantasma());
                else
                    network.addUniEdge(obj_aposento.getNome(), obj_ligacao, 0);
            }
        }

    }

    /**
     * Toca musica durante o jogo
     */
    @Override
    public void startMusic() {
        try {
            clip = AudioSystem.getClip();
            AudioInputStream ais = AudioSystem.getAudioInputStream(new File("files/scary_skeleton.wav")); //ficheiro de musica
            clip.open(ais);
            clip.loop(Clip.LOOP_CONTINUOUSLY); //toca a musica em loop ate o programa acabar
        } catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
            System.out.println("Erro no metodo startMusic()");
        }
    }

    /**
     * Metodo que devolve uma ArrayUnorderedList das ligacoes do aposento recebido
     *
     * @param aposento nome do aposento
     * @return ligacoes ao aposento recebido
     */
    @Override
    public ArrayUnorderedList<String> findLigacoes(String aposento) {
        return network.getLigacoes(aposento);
    }

    /**
     * Metodo verifica se o aposento contem um fantasma
     *
     * @param aposento nome do aposento
     * @return valor dos pontos do fantasma
     */
    @Override
    public int checkGhost(String aposento) {
        return network.getWeight(aposento);
    }

    /**
     * Metodo que remove pontos ao jogador
     *
     * @param dano valor de pontos a remover aos pontos do jogador
     */
    public void removerPontos(int dano) {
        pontos_jogador -= dano;
        if (pontos_jogador < 0) {
            pontos_jogador = 0;
        }
    }

    /**
     * Metodo que carrega as classificacoes guardadas em ficheiro para um array
     */
    public void carregaClassificacoes() {
        File file = new File("files/saveFile.csv"); //ficheiro com as classificacoes
        this.classificacoes = new ArrayOrderedList<>(); //inicializa array
        try {
            Scanner scanner = new Scanner(file); //scanner para ler o ficheiro

            scanner.nextLine(); //passa o header a frente
            while (scanner.hasNextLine()) {
                String[] classificacao = scanner.nextLine().split(";"); //split da string
                classificacoes.add(new Utilizador(classificacao[0], Integer.parseInt(classificacao[1]), classificacao[2], new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(classificacao[3]), Double.parseDouble(classificacao[4]), Integer.parseInt(classificacao[5]), Integer.parseInt(classificacao[6])));
            }
            scanner.close();

            System.out.println("Classificacoes carregadas com sucesso.");
        } catch (FileNotFoundException e) {
            System.out.println("Erro no metodo readSaveFile() - Ficheiro nao existe");
        } catch (Exception e) {
            System.out.println("Erro no metodo readSaveFile()");
        }
    }

    /**
     * Metodo que guarda as classificacoes num ficheiro
     */
    public void guardarClassificacoes() {
        File file = new File("files/saveFile.csv"); //ficheiro com as classificacoes
        try {
            FileWriter fileWriter = new FileWriter(file);
            //converte o array de classificacoes numa string
            fileWriter.write("nome,pontos,mapa,data,tempo,dificuldade,movimentos"); //escreve header no ficheiro
            for (Utilizador classificacao : classificacoes) {
                fileWriter.write( "\n"+ classificacao.getNome() + ";" + classificacao.getPontos() + ";" + classificacao.getMapa() + ";" + new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(classificacao.getData()) + ";" + classificacao.getTempo() + ";" + classificacao.getDificuldade() + ";" + classificacao.getMovimentos());
            }
            fileWriter.close();

            System.out.println("Classificacoes guardas com sucesso.");
        } catch (FileNotFoundException e) {
            System.out.println("Erro no metodo exportSaves() - File not found");
        } catch (Exception e) {
            System.out.println("Erro no metodo exportSaves()");
        }
    }

}