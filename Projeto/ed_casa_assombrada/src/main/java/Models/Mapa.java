package Models;

import Ficha_12.Ex_4.ArrayUnorderedList.ArrayUnorderedList;

/**
 * Classe que permite criar um mapa
 */
public class Mapa {

    //Nome do mapa
    private String nome;
    //Numero de pontos do mapa
    private int pontos;
    //Array de aposentos do mapa
    private ArrayUnorderedList<Aposento> aposentos;

    /**
     * Metodo construtor que inicializa um mapa com nome, numero de pontos e array de aposentos
     *
     * @param nome      nome do mapa
     * @param pontos    numero de pontos de um mapa
     * @param aposentos lista de aposentos do mapa
     */
    public Mapa(String nome, int pontos, ArrayUnorderedList<Aposento> aposentos) {
        this.nome = nome;
        this.pontos = pontos;
        this.aposentos = aposentos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public ArrayUnorderedList<Aposento> getAposentos() {
        return aposentos;
    }

    public void setAposentos(ArrayUnorderedList<Aposento> aposentos) {
        this.aposentos = aposentos;
    }

    /**
     * Metodo que retorna uma string com informação sobre o Mapa
     *
     * @return uma string representativa do mapa
     */
    @Override
    public String toString() {
        return "Mapa{" +
                "nome='" + nome + '\'' +
                ", pontos=" + pontos +
                ", aposentos=" + aposentos +
                '}';
    }
}
