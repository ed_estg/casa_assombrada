package Models;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe que permite criar um utilizador
 */

public class Utilizador {

    private String nome;
    //Numero de pontos do utilizador
    private int pontos;
    //Nome do mapa que o utilizador jogou
    private String mapa;
    //Data em que o utilizador joga o jogo
    private Date data;
    //Tempo que o utilizador demora a acabar o jogo
    private double tempo;
    //Nivel de dificuldade do mapa
    private int dificuldade;
    //Numero de movimentos que um utilizador faz durante um jogo
    private int movimentos;

    /**
     * Metodo construtor que inicializa um utilizador com um nome, o numero de pontos, o mapa, a data, o tempo, a dificuldade
     * e o numero de movimentos que um jogador faz durante um jogo
     *
     * @param nome        nome do jogador
     * @param pontos      pontos com que o jogador fica no final do jogo
     * @param mapa        mapa do jogo
     * @param data        data do jogo
     * @param tempo       tempo que o jogaador demorou a jogar o jogo
     * @param dificuldade nível de dificuldade do mapa
     * @param movimentos  número de movimentos que um jogador faz durante um jogo
     */
    public Utilizador(String nome, int pontos, String mapa, Date data, double tempo, int dificuldade, int movimentos) {
        this.nome = nome;
        this.pontos = pontos;
        this.mapa = mapa;
        this.data = data;
        this.tempo = tempo;
        this.dificuldade = dificuldade;
        this.movimentos = movimentos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public String getMapa() {
        return mapa;
    }

    public void setMapa(String mapa) {
        this.mapa = mapa;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getTempo() {
        return tempo;
    }

    public void setTempo(double tempo) {
        this.tempo = tempo;
    }

    public int getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(int dificuldade) {
        this.dificuldade = dificuldade;
    }

    public int getMovimentos() {
        return movimentos;
    }

    public void setMovimentos(int movimentos) {
        this.movimentos = movimentos;
    }

    /**
     * Metodo que retorna uma string com informaçao sobre o Utilizador
     *
     * @return uma string com os dados do Utilizador
     */
    @Override
    public String toString() {
        return "Utilizador: " + nome +
                ", " + pontos +
                " pontos, tempo: " + tempo +
                ", dificuldade: " + dificuldade +
                ", " + movimentos +
                " movimentos, data: " + new SimpleDateFormat("dd-MM-yyyy HH:mm").format(data);
    }
}