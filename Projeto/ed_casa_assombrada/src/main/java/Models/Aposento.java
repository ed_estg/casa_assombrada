package Models;

import Ficha_12.Ex_4.ArrayUnorderedList.ArrayUnorderedList;

/**
 * Classe que permite criar um aposento
 */
public class Aposento {

    //Nome do Aposento
    private String nome;
    //Pontos do Fantasma
    private int fantasma;
    //Array de ligaçoes que o aposento tem
    private ArrayUnorderedList<String> ligacoes;

    /**
     * Metodo construtor que inicializa um aposento com um nome, o numero de pontos e o array de ligaçoes para esse
     * aposento
     *
     * @param nome     nome do aposento
     * @param fantasma numero de pontos do fantasma
     * @param ligacoes Array de ligaçoes com as ligaçoes possiveis para o aposento
     */
    public Aposento(String nome, int fantasma, ArrayUnorderedList<String> ligacoes) {
        this.nome = nome;
        this.fantasma = fantasma;
        this.ligacoes = ligacoes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getFantasma() {
        return fantasma;
    }

    public void setFantasma(int fantasma) {
        this.fantasma = fantasma;
    }

    public ArrayUnorderedList<String> getLigacoes() {
        return ligacoes;
    }

    public void setLigacoes(ArrayUnorderedList<String> ligacoes) {
        this.ligacoes = ligacoes;
    }

    /**
     * Metodo que retorna uma string com informaçao sobre o aposento nomeadamente o nome, os pontos do fantasma e
     * as ligaçoes do aposento
     *
     * @return uma string representativa do aposento
     */
    @Override
    public String toString() {
        return "Aposento{" +
                "nome='" + nome + '\'' +
                ", fantasma=" + fantasma +
                ", ligacoes=" + ligacoes +
                '}';
    }
}
