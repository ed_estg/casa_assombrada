package Interfaces;

import Ficha_12.Ex_4.ArrayUnorderedList.ArrayUnorderedList;

public interface JanelaJogoADT {

    /**
     * Define o tamanho, localizacao e layout da janela
     */
    public void definirConteudo();

    /**
     * Define a imagem de fundo da janela e o nome
     *
     * @param file_path caminho pra a imagem
     * @param nome      nome do aposento
     */
    public void setImage(String file_path, String nome);

    /**
     * Método que permite criar um botão para que o utilizador possa desistir do jogo
     */
    public void mostrarBotaoDesistir();

    /**
     * Método que permite criar uma label com a imagem do fantasma
     */
    public void mostrarFantasma();

    /**
     * Método que permite criar uma barra com os pontos do jogador
     */
    public void mostrarBarraPontos();

    /**
     * Método que permite criar botoes para todos os aposentos
     *
     * @param aposentos array de aposentos
     */
    public void setBotoes(ArrayUnorderedList<String> aposentos);

}

