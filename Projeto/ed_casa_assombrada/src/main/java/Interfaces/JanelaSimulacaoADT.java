package Interfaces;

import Exececoes.EmptyException;

import java.util.Iterator;

public interface JanelaSimulacaoADT {

    /**
     * Define o tamanho, localizacao e layout da janela
     */
    public void definirConteudo();

    /**
     * Metodo de devolve uma string para ser mostrada numa janela com html de forma a imprimir uma tabela com a matriz
     * de ligacoes da network
     *
     * @return string da matriz em forma de tabale usando html
     */
    public String getTabelaGrafo();

    /**
     * Método que devolve um iterador com o caminho mais curto e com menos dano
     *
     * @return iterador com o caminho mais curto e com menos dano
     * @throws EmptyException
     */
    public Iterator getMelhorCaminhoIterator() throws EmptyException;

    /**
     * Método permite encontrar o caminho mais curto e com menos dano
     *
     * @return uma string com o caminho mais curto e com menos dano
     */
    public String getMelhorCaminho();

    /**
     * Método que devolve os pesos e as ligacoes adjacentes
     *
     * @return uma string com os os pesos e as ligacoes adjacentes
     */
    public String getAdjacencias();
}
