package Interfaces;

import Ficha_12.Ex_4.ArrayUnorderedList.ArrayUnorderedList;

public interface GestorADT {

    /**
     * Metodo para adicionar classificacao ao array e guardar no ficheiro
     *
     * @param nome_jogador   nome do jogador
     * @param nome_mapa      nome do mapa
     * @param dificuladade   nivel de dificuldade jogado
     * @param num_movimentos numero de movimentos feitos
     */
    public void adicionarClassificacao(String nome_jogador, String nome_mapa, int dificuladade, int num_movimentos);

    /**
     * Le o ficheiro JSON e inicializa o mapa e com os dados do ficheiro JSON
     *
     * @param file_path - caminho do ficheiro a ler
     * @return sucesso ou insucesso da operacao
     */
    public boolean readJson(String file_path);

    /**
     * Toca musica durante o jogo
     */
    public void startMusic();

    /**
     * Metodo que devolve uma ArrayUnorderedList das ligacoes do aposento recebido
     *
     * @param aposento nome do aposento
     * @return ligacoes ao aposento recebido
     */
    public ArrayUnorderedList<String> findLigacoes(String aposento);

    /**
     * Metodo verifica se o aposento contem um fantasma
     *
     * @param aposento nome do aposento
     * @return valor dos pontos do fantasma
     */
    public int checkGhost(String aposento);

    /**
     * Metodo que remove pontos ao jogador
     *
     * @param dano valor de pontos a remover aos pontos do jogador
     */
    public void removerPontos(int dano);


}
