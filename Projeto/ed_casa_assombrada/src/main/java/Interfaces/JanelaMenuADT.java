package Interfaces;


public interface JanelaMenuADT {

    /**
     * Método que adiciona o conteúdo a janela do Menu
     */
    public void adicionarConteudo();

    /**
     * Método que mostra uma label e um textField para introduzir o nome do Jogador
     */
    public void readNomeJogador();

    /**
     * Método que mostra uma combobox para escolher o mapa
     * Vai buscar as opções a pasta files e procura por ficheiros do tipo json
     */
    public void escolherMapa();

    /**
     * Método que mostra o botão para começar o modo Manual
     */
    public void botaoModoManual();

    /**
     * Método que mostra o botão para começar o modo Simulação
     */
    public void botaoModoSimulacao();

    /**
     * Método para mostrar as opções de dificuldade
     * Opçõess: Basico, Normal, Dificil
     */
    public void botoesDificuldade();

    /**
     * Método com um botão que mastra as regras do jogo
     */
    public void botaoRegras();

    /**
     * Método que cria um botão para ver as classificações do jogadores
     */
    public void botaoClassificacoes();

    /**
     * Método que cria um botão pra sair do programa
     */
    public void botaoSair();

}
