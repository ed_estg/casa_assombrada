package Estruturas;

import Exececoes.EmptyException;
import Ficha_12.Ex_4.ArrayUnorderedList.ArrayUnorderedList;
import Ficha_12.Ex_4.LinkedQueue.LinkedQueue;
import Ficha_12.Ex_4.LinkedStack.EmptyStackException;
import Ficha_12.Ex_4.LinkedStack.LinkedStack;
import Ficha_12.Ex_4.Node.LinearNode;
import Interfaces.GraphADT;

import java.io.Serializable;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe Grafo que gere as ligações e arestas do grafo
 */
public class Graph<T> implements GraphADT<T>, Serializable {

    //Number of vertices in the graph
    protected int numVertices;
    //Adjacency matrix of the graph
    protected boolean[][] adjMatrix;
    //Array with the values of the vertices
    protected T[] vertices;

    /**
     * Creates a new empty graph.
     */
    Graph() {
        numVertices = 0;
        this.adjMatrix = new boolean[0][0];
        this.vertices = (T[]) (new Object[0]);
    }

    /**
     * Devolve o valor do indice do array de vertices
     *
     * @param vertex vertice que pretenda obter o indice
     * @return indice do vertice
     */
    public int getIndex(T vertex) {
        for (int i = 0; i < size(); i++) {
            if (vertices[i].equals(vertex)) {
                return i;
            }
        }
        throw new ArrayIndexOutOfBoundsException("Vertex doesn't exists");
    }

    /**
     * Procura se o index pode existir no array
     *
     * @param index index para verificar
     * @return boolean, true se for valido e false se nao for valido
     */
    boolean indexIsValid(int index) {
        return (index < size()) && (index >= 0);
    }

    /**
     * Expande a capacidade do array e da matriz
     */
    protected void expandCapacity() {
        boolean[][] temp1 = new boolean[vertices.length + 1][vertices.length + 1];
        T[] temp = (T[]) (new Object[vertices.length + 1]);

        System.arraycopy(vertices, 0, temp, 0, numVertices);
        for (int x = 0; x < numVertices; x++) {
            System.arraycopy(adjMatrix[x], 0, temp1[x], 0, numVertices);
        }

        vertices = temp;
        adjMatrix = temp1;
    }

    /**
     * Insere uma aresta bidirecional entre dois vertices do grafo
     *
     * @param vertex1 o primeiro vertice
     * @param vertex2 o segundo vertice
     */
    @Override
    public void addEdge(T vertex1, T vertex2) {
        addEdge(getIndex(vertex1), getIndex(vertex2));
    }

    /**
     * Insere uma aresta bidirecional entre os dois vertices do grafo, e define a ligação na matris de adjacencia
     *
     * @param index1 o indice do primeiro vertice
     * @param index2 o indice do segundo vertice
     */
    private void addEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = true;
            adjMatrix[index2][index1] = true;
        }
    }

    /**
     * Insere uma aresta unidirecional entre dois vertices do grafo
     * Regista a ligacao na matriz de adjacencia
     *
     * @param index1 o indice do primeiro vertice
     * @param index2 o indice do segundo vertice
     */
    private void addUniEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = true;
        }
    }

    /**
     * Insere uma aresta unidirecional entre 2 vertices do grafo
     *
     * @param vertex1 o primeiro vertice
     * @param vertex2 o segundo vertice
     */
    void addUniEdge(T vertex1, T vertex2) {
        addUniEdge(getIndex(vertex1), getIndex(vertex2));
    }

    /**
     * Remove a areste bidirecional entre 2 vertices
     *
     * @param vertex1 primeiro vertice da aresta
     * @param vertex2 segundo vertice da aresta
     * @throws EmptyException se o grafo estiver vazio
     */
    @Override
    public void removeEdge(T vertex1, T vertex2) throws EmptyException {
        if (isEmpty()) {
            throw new EmptyException("Empty Graph");
        }
        removeEdge(getIndex(vertex1), getIndex(vertex2));
    }

    /**
     * Remove a aresta entre 2 vertices
     *
     * @param index1 o indice do primeiro vertice da aresta
     * @param index2 o indice do segundo vertice da aresta
     */
    private void removeEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = false;
            adjMatrix[index2][index1] = false;
        }
    }

    /**
     * Remove a aresta unidirecional entre os dois vertices
     *
     * @param vertex1 primeiro vertice da aresta
     * @param vertex2 segundo vertice da aresta
     * @throws EmptyException se o grafo estiver vazio
     */
    public void removeUniEdge(T vertex1, T vertex2) throws EmptyException {
        if (isEmpty()) {
            throw new EmptyException("Empty Graph");
        }
        removeUniEdge(getIndex(vertex1), getIndex(vertex2));
    }

    /**
     * Remove a aresta unidirecional entre os dois vertices
     *
     * @param index1 indice do primeiro vertice da aresta
     * @param index2 indice do segundo vertice da aresta
     */
    private void removeUniEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = false;
        }
    }

    /**
     * Adiciona o vertice ao grafo, expande a capacidade se necessario
     *
     * @param vertex vertice a ser adicionado ao grafo
     */
    @Override
    public void addVertex(T vertex) {
        if (numVertices == vertices.length) {
            expandCapacity();
        }
        vertices[numVertices] = vertex;
        for (int i = 0; i <= numVertices; i++) {
            adjMatrix[numVertices][i] = false;
            adjMatrix[i][numVertices] = false;
        }
        numVertices++;
    }

    /**
     * Remove o vertice do grafo
     *
     * @param vertex vertice a ser removido
     * @throws EmptyException se o grafo estiver vazio
     */
    @Override
    public void removeVertex(T vertex) throws EmptyException {
        if (isEmpty()) {
            throw new EmptyException("Empty Graph");
        }
        removeVertex(getIndex(vertex));
    }

    /**
     * Remove o vertice do grafo
     *
     * @param index indice do vertice a remover
     */
    private void removeVertex(int index) {
        if (indexIsValid(index)) {
            numVertices--;
            if (numVertices - index >= 0) System.arraycopy(vertices, index + 1, vertices, index, numVertices - index);
            for (int i = index; i < numVertices; i++) {
                if (numVertices + 1 >= 0) System.arraycopy(adjMatrix[i + 1], 0, adjMatrix[i], 0, numVertices + 1);
            }
            for (int i = index; i < numVertices; i++) {
                for (int j = 0; j < numVertices; j++) {
                    adjMatrix[j][i] = adjMatrix[j][i + 1];
                }
            }
        }
    }

    /**
     * Devolve um iterador que faz uma breve primeira procura transversal a começar no indice recebido
     *
     * @param startVertex vertice onde começa a procura
     * @return um iterador que faz uma breve primeira procura transversal
     * @throws EmptyException se o grafo estiver vazio
     */
    @Override
    public Iterator iteratorBFS(T startVertex) throws EmptyException {
        if (isEmpty()) {
            throw new EmptyException("Empty Graph");
        }
        return iteratorBFS(getIndex(startVertex));
    }

    /**
     * Devolve um iterador que faz uma breve primeira procura transversal a começar no indice recebido
     *
     * @param startIndex indice do vertice onde começa a procura
     * @return um iterador que faz uma breve primeira procura transversal
     */
    protected Iterator<T> iteratorBFS(int startIndex) {
        Integer x;
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }
        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;
        while (!traversalQueue.isEmpty()) {
            x = traversalQueue.dequeue();
            resultList.addToRear(vertices[x]);

            //procura todos os vertices adjacentes a x que ainda nao foram visitados e coloca-os na fila
            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[x][i] && !visited[i]) {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }
            }
        }
        return resultList.iterator();
    }

    /**
     * Devolve um iterador que faz uma procura em profindade transversal a começar no indice recebido
     *
     * @param startVertex vertice onde começa
     * @return um iterador que faz uma procura em profindade transversal
     */
    @Override
    public Iterator iteratorDFS(T startVertex) throws EmptyException, EmptyStackException {
        if (isEmpty()) {
            throw new EmptyException("empty");
        }
        return iteratorDFS(getIndex(startVertex));
    }

    /**
     * Devolve um iterador que faz uma procura em profindade transversal a começar no indice recebido
     *
     * @param startIndex indice do vertice onde começa
     * @return um iterador que faz uma procura em profindade transversal
     */
    protected Iterator<T> iteratorDFS(int startIndex) throws EmptyStackException {
        Integer x;
        boolean found;
        LinkedStack<Integer> traversalStack = new LinkedStack<>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        boolean[] visited = new boolean[numVertices];
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalStack.push(new LinearNode<>(startIndex));
        resultList.addToRear(vertices[startIndex]);
        visited[startIndex] = true;
        while (!traversalStack.isEmpty()) {
            x = traversalStack.peek();
            found = false;

            for (int i = 0; (i < numVertices) && !found; i++) {
                if (adjMatrix[x][i] && !visited[i]) {
                    traversalStack.push(new LinearNode<>(i));
                    resultList.addToRear(vertices[i]);
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                try {
                    traversalStack.pop();
                } catch (Exception ex) {
                    Logger.getLogger(Graph.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultList.iterator();
    }

    /**
     * Metodo que cria um iterador com o caminho mais curto
     *
     * @param startVertex  vertice inicial
     * @param targetVertex vertice final
     * @return um iterador com o caminho mais curto
     * @throws EmptyException se o grafo estiver vazio
     */
    @Override
    public Iterator<T> iteratorShortestPath(T startVertex, T targetVertex) throws EmptyException {
        if (isEmpty()) {
            throw new EmptyException("Empty Graph");
        }
        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }

    /**
     * Metodo que cria um iterador para o caminho mais curto
     *
     * @param startIndex  indice do vertice inicial
     * @param targetIndex indice do vertice final
     * @return um iterador com o caminho mais curto
     * @throws EmptyException se o grafo estiver vazio
     */
    protected Iterator<T> iteratorShortestPath(int startIndex, int targetIndex) throws EmptyException {

        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return resultList.iterator();
        }

        Iterator<Integer> it = iteratorShortestPathIndex(startIndex, targetIndex);
        while (it.hasNext()) {
            resultList.addToRear(vertices[it.next()]);
        }
        return resultList.iterator();
    }

    /**
     * Iterador para o caminho mais curto
     *
     * @param startIndex  index do vertice onde começa
     * @param targetIndex index do vertice que pretende alcançar
     * @return iterador com os vertices para o caminho mais curto
     */
    private Iterator<Integer> iteratorShortestPathIndex(int startIndex, int targetIndex) {
        int index = startIndex;
        int[] predecessor = new int[numVertices];
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();
        ArrayUnorderedList<Integer> resultList = new ArrayUnorderedList<>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) || (startIndex == targetIndex)) {
            return resultList.iterator();
        }
        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }
        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;
        predecessor[startIndex] = -1;
        while (!traversalQueue.isEmpty() && (index != targetIndex)) {
            index = traversalQueue.dequeue();
            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[index][i] && !visited[i]) {
                    predecessor[i] = index;
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }
            }
        }
        if (index != targetIndex) {
            return resultList.iterator();
        }
        LinkedStack<Integer> stack = new LinkedStack<>();
        index = targetIndex;
        stack.push(new LinearNode<>(index));
        do {
            index = predecessor[index];
            stack.push(new LinearNode<>(index));
        } while (index != startIndex);
        while (!stack.isEmpty()) {
            resultList.addToRear(stack.pop());
        }
        return resultList.iterator();
    }

    /**
     * Devolve true se o grafo estiver vazio
     *
     * @return boolean, true se o grafo estiver vazio, false se nao estiver vazio
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Devolve o tamanho do grafo
     *
     * @return tamanho do array
     */
    @Override
    public int size() {
        return this.numVertices;
    }

    /**
     * Devolve true se o grafo estiver conectado, e false se nao estiver
     *
     * @return true se o grafo estiver conectado, e false se nao estiver
     */
    @Override
    public boolean isConnected() {
        if (isEmpty()) {
            return false;
        }

        Iterator<T> it = iteratorBFS(0);
        int count = 0;

        while (it.hasNext()) {
            it.next();
            count++;
        }
        return (count == numVertices);
    }
}
