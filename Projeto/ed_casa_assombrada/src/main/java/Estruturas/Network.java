package Estruturas;

import Exececoes.EmptyException;
import Ficha_12.Ex_4.ArrayUnorderedList.ArrayUnorderedList;
import Ficha_12.Ex_4.LinkedStack.EmptyStackException;
import Ficha_11.Ex_3.LinkedHeap;
import Ficha_12.Ex_4.LinkedQueue.LinkedQueue;
import Ficha_12.Ex_4.LinkedStack.LinkedStack;
import Ficha_12.Ex_4.Node.LinearNode;
import Interfaces.NetworkADT;

import java.io.Serializable;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe Network que permite definir os vertices e as arestas de um grafo
 */
public class Network<T> extends Graph<T> implements NetworkADT<T>, Serializable {
    //Adjacency matrix
    private double[][] wAdjMatrix;

    /**
     * Cria uma network vazia
     */
    public Network() {
        super();
        numVertices = 0;
        this.wAdjMatrix = new double[0][0];
        this.vertices = (T[]) (new Object[0]);
    }

    /**
     * Adiciona uma aresta bidirecional à network
     *
     * @param index1 indice do primeiro vertice
     * @param index2 indice do segundo vertice
     * @param weight peso da ligação
     */
    private void addEdge(int index1, int index2, double weight) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            wAdjMatrix[index1][index2] = weight;
            wAdjMatrix[index2][index1] = weight;

        }
    }

    /**
     * Adiciona uma aresta unidirecional à network
     *
     * @param index1 indice do primeiro vertice
     * @param index2 indice do segundo vertice
     * @param weight peso da ligação
     */
    private void addUniEdge(int index1, int index2, double weight) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            wAdjMatrix[index1][index2] = weight;
        }
    }

    /**
     * Adiciona uma aresta bidirecional à network
     *
     * @param vertex1 primeiro vertice
     * @param vertex2 segundo vertice
     * @param weight peso da ligação
     */
    @Override
    public void addEdge(T vertex1, T vertex2, double weight) {
        addEdge(getIndex(vertex1), getIndex(vertex2), weight);
        super.addEdge(vertex1, vertex2);
    }

    /**
     * Adiciona uma aresta unidirecional à network
     *
     * @param vertex1 primeiro vertice
     * @param vertex2 segundo vertice
     * @param weight peso da ligação
     */
    public void addUniEdge(T vertex1, T vertex2, double weight) {
        addUniEdge(getIndex(vertex1), getIndex(vertex2), weight);
        super.addUniEdge(vertex1, vertex2);
    }

    /**
     * Remove a aresta unidirecional da network
     *
     * @param vertex1 primeiro vertice da aresta
     * @param vertex2 segundo vertice da aresta
     */
    @Override
    public void removeUniEdge(T vertex1, T vertex2) throws EmptyException {
        removeUniEdge(getIndex(vertex1), getIndex(vertex2));
        super.removeUniEdge(vertex1, vertex2);
    }

    /**
     * Remove a aresta unidirecional da network
     *
     * @param index1 indice do primeiro vertice da aresta
     * @param index2 indice do segundo vertice da aresta
     */
    private void removeUniEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            wAdjMatrix[index1][index2] = Double.POSITIVE_INFINITY;
        }
    }

    /**
     * Remove a aresta bidirecional da network
     *
     * @param vertex1 primeiro vertice da aresta
     * @param vertex2 segundo vertice da aresta
     */
    @Override
    public void removeEdge(T vertex1, T vertex2) throws EmptyException {
        removeEdge(getIndex(vertex1), getIndex(vertex2));
        super.removeEdge(vertex1, vertex2);
    }

    /**
     * Remove a aresta bidirecional da network
     *
     * @param index1 indice do primeiro vertice da aresta
     * @param index2 indice do segundo vertice da aresta
     */
    private void removeEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            wAdjMatrix[index1][index2] = Double.POSITIVE_INFINITY;
            wAdjMatrix[index2][index1] = Double.POSITIVE_INFINITY;
        }
    }

    /**
     * Expande o array e a matriz
     */
    @Override
    protected void expandCapacity() {
        double[][] largerWAdjMatrix = new double[vertices.length + 1][vertices.length + 1];
        for (int i = 0; i < numVertices; i++) {
            System.arraycopy(wAdjMatrix[i], 0, largerWAdjMatrix[i], 0, numVertices);
        }
        wAdjMatrix = largerWAdjMatrix;

        super.expandCapacity();
    }

    /**
     * Adiciona o vertice à network
     *
     * @param vertex vertice a adicionar
     */
    @Override
    public void addVertex(T vertex) {
        if (numVertices == vertices.length) {
            expandCapacity();
        }

        vertices[numVertices] = vertex;
        for (int i = 0; i <= numVertices; i++) {
            wAdjMatrix[numVertices][i] = Double.POSITIVE_INFINITY;
            wAdjMatrix[i][numVertices] = Double.POSITIVE_INFINITY;
        }
        numVertices++;
    }

    /**
     * Remove o vertice da network
     *
     * @param index indice do vertice a remover
     */
    public void removeVertex(int index) {
        if (indexIsValid(index)) {
            numVertices--;

            if (numVertices - index >= 0) System.arraycopy(vertices, index + 1, vertices, index, numVertices - index);

            for (int i = index; i < numVertices; i++) {
                if (numVertices + 1 >= 0) System.arraycopy(wAdjMatrix[i + 1], 0, wAdjMatrix[i], 0, numVertices + 1);
            }

            for (int i = index; i < numVertices; i++) {
                for (int j = 0; j < numVertices; j++) {
                    wAdjMatrix[j][i] = wAdjMatrix[j][i + 1];
                }
            }
        }
    }

    /**
     * Devolve um iterador com o caminho mais curto do grafo
     *
     * @param startVertex  vertice inicial
     * @param targetVertex vertice final
     * @return um iterador com o caminho mais curto do grafo
     * @throws EmptyException se o grafo estiver vazio
     */
    public Iterator<T> iteratorShortestPath(T startVertex, T targetVertex) throws EmptyException {
        if (isEmpty()) {
            throw new EmptyException("Empty Graph");
        }
        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }

    /**
     * Devolve um iterador com o caminho mais curto do grafo
     *
     * @param startIndex,  o indice do vertice inicial
     * @param targetIndex, o indice do vertice final
     * @return um iterador com os vertices que definem o caminho mais curto
     * @throws EmptyException, se grafo estiver vazio
     */
    protected Iterator<T> iteratorShortestPath(int startIndex, int targetIndex) throws EmptyException {

        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return resultList.iterator();
        }

        Iterator it = iteratorShortestPathIndices(startIndex, targetIndex);
        while (it.hasNext()) {
            resultList.addToRear(vertices[(int) it.next()]);
        }
        return resultList.iterator();
    }

    /**
     * Devolve um iterador que contem os indices dos vertices com o caminho mais "curto" entre os vertices recebidos
     *
     * @param startIndex  indice do vertice inicial
     * @param targetIndex indice do vertice que prentende atingir
     * @return um iterador que contem os indices dos vertices com o caminho mais "curto"
     */
    public Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) throws EmptyException {
        int index;
        double weight;
        int[] predecessor = new int[numVertices];
        LinkedHeap<Double> traversalMinHeap = new LinkedHeap<>();
        ArrayUnorderedList<Integer> resultList = new ArrayUnorderedList<>();
        LinkedStack<Integer> stack = new LinkedStack<>();

        double[] pathWeight = new double[numVertices];
        for (int i = 0; i < numVertices; i++) {
            pathWeight[i] = Double.POSITIVE_INFINITY;
        }

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) || (startIndex == targetIndex) || isEmpty()) {
            return resultList.iterator();
        }

        pathWeight[startIndex] = 0;
        predecessor[startIndex] = -1;
        visited[startIndex] = true;

        //atualiza o pathWeight de cada vertice
        for (int i = 0; i < numVertices; i++) {
            if (!visited[i]) {
                pathWeight[i] = pathWeight[startIndex] + wAdjMatrix[startIndex][i];
                predecessor[i] = startIndex;
                traversalMinHeap.addElement(pathWeight[i]);
            }
        }

        do {
            weight = traversalMinHeap.removeMin();
            traversalMinHeap.removeAllElements();
            if (weight == Double.POSITIVE_INFINITY) //sem caminho possivel
            {
                return resultList.iterator();
            } else {
                index = getIndexOfAdjVertexWithWeightOf(visited, pathWeight, weight);
                visited[index] = true;
            }

            //atualiza o pathWeight de cada vertice
            for (int i = 0; i < numVertices; i++) {
                if (!visited[i]) {
                    if ((wAdjMatrix[index][i] < Double.POSITIVE_INFINITY) && (pathWeight[index] + wAdjMatrix[index][i]) < pathWeight[i]) {
                        pathWeight[i] = pathWeight[index] + wAdjMatrix[index][i];
                        predecessor[i] = index;
                    }
                    traversalMinHeap.addElement(pathWeight[i]);
                }
            }
        } while (!traversalMinHeap.isEmpty() && !visited[targetIndex]);

        index = targetIndex;
        stack.push(new LinearNode<>(index));
        do {
            index = predecessor[index];
            stack.push(new LinearNode<>(index));
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addToRear((stack.pop()));
        }
        return resultList.iterator();
    }

    /**
     * Devolve o indice do vertice que é adjacente ao vertice com o indice recebido e o mesmo peso
     *
     * @param visited    se for true o vertice ja foi visitado
     * @param pathWeight pesos do caminho
     * @param weight     peso da ligacao
     * @return indice do vertice que é adjacente ao vertice com o indice recebido
     */
    private int getIndexOfAdjVertexWithWeightOf(boolean[] visited, double[] pathWeight, double weight) {
        for (int i = 0; i < numVertices; i++) {
            if ((pathWeight[i] == weight) && !visited[i]) {
                return i;
            }
        }
        System.out.println("erro");
        return -1;
    }

    /**
     * Retorna o peso do caminho de menor peso na rede
     * Devolve infinito positivo se nenhum caminho for encontrado
     *
     * @param startIndex  indice do vertice inicial
     * @param targetIndex indice do vertice alvo
     * @return peso do caminho de menor peso na rede ou infinito positivo se nenhum caminho for encontrado
     */
    private double shortestPathWeight(int startIndex, int targetIndex) throws EmptyException {
        double result = 0;
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return Double.POSITIVE_INFINITY;
        }

        int index1, index2;
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);

        if (it.hasNext()) {
            index1 = it.next();
        } else {
            return Double.POSITIVE_INFINITY;
        }

        while (it.hasNext()) {
            index2 = it.next();
            result += wAdjMatrix[index1][index2];
            index1 = index2;
        }
        return result;
    }

    /**
     * Retorna o peso do caminho de menor peso na rede
     * Devolve infinito positivo se nenhum caminho for encontrado
     *
     * @param startVertex  vertice inicial
     * @param targetVertex vertice alvo
     * @return peso do caminho de menor peso na rede ou infinito positivo se nenhum caminho for encontrado
     */
    @Override
    public double shortestPathWeight(T startVertex, T targetVertex) {
        try {
            return shortestPathWeight(getIndex(startVertex), getIndex(targetVertex));
        } catch (EmptyException ex) {
            throw new NullPointerException("Colecao esta vazia.");
        }
    }

    /**
     * Devolve os pesos da matriz adjacente
     *
     * @return matriz
     */
    public double[][] getwAdjMatrix() {
        return wAdjMatrix;
    }

    /**
     * Retorna um iterador que executa uma primeira travessia de pesquisa transversal
     * começando no índice fornecido.
     *
     * @param startIndex indice do vertice onde começa
     * @return iterador que executa uma primeira travessia transversal
     */
    @Override
    protected Iterator<T> iteratorBFS(int startIndex) {
        Integer x;
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }
        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;
        while (!traversalQueue.isEmpty()) {
            x = traversalQueue.dequeue();
            resultList.addToRear(vertices[x]);
            //Procura todos os vertices ligados a x que ainda nao foram visitados e coloca-os na fila
            for (int i = 0; i < numVertices; i++) {
                if (wAdjMatrix[x][i] != Double.POSITIVE_INFINITY && !visited[i]) {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }
            }
        }
        return resultList.iterator();
    }

    /**
     * Retorna um iterador que executa uma primeira travessia de pesquisa transversal
     * começando no índice fornecido.
     *
     * @param startVertex vertice onde começa
     * @return iterador que executa uma primeira travessia transversal
     * @throws EmptyException se o grafo estiver vazio
     */
    @Override
    public Iterator iteratorBFS(T startVertex) throws EmptyException {
        if (isEmpty()) {
            throw new EmptyException("Empty Graph");
        }
        return iteratorBFS(getIndex(startVertex));
    }

    /**
     * Devolve um iterador que faz uma procura inicial profunda transversal a começar no vertice recebido
     *
     * @param startVertex o vertice inicial
     * @return um iterador que faz uma procura inicial profunda transversal
     */
    @Override
    public Iterator iteratorDFS(T startVertex) throws EmptyException, EmptyStackException {
        if (isEmpty()) {
            throw new EmptyException("empty");
        }
        return iteratorDFS(getIndex(startVertex));
    }

    /**
     * Devolve um iterador que faz uma procura inicial profunda transversal a começar no vertice recebido
     *
     * @param startIndex indice do vertice inicial
     * @return um iterador que faz uma procura inicial profunda transversal
     */
    @Override
    protected Iterator<T> iteratorDFS(int startIndex) throws EmptyStackException {
        Integer x;
        boolean found;
        LinkedStack<Integer> traversalStack = new LinkedStack<>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        boolean[] visited = new boolean[numVertices];
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalStack.push(new LinearNode<>(startIndex));
        resultList.addToRear(vertices[startIndex]);
        visited[startIndex] = true;
        while (!traversalStack.isEmpty()) {
            x = traversalStack.peek();
            found = false;

            for (int i = 0; (i < numVertices) && !found; i++) {
                if (wAdjMatrix[x][i] != Double.POSITIVE_INFINITY && !visited[i]) {
                    traversalStack.push(new LinearNode<>(i));
                    resultList.addToRear(vertices[i]);
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                try {
                    traversalStack.pop();
                } catch (Exception ex) {
                    Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return resultList.iterator();
    }

    /**
     * Devolve o index do vertice
     *
     * @param vertex vertice
     * @return index do vertice
     */
    @Override
    public int getIndex(T vertex) {
        for (int i = 0; i < size(); i++) {
            if (vertices[i].equals(vertex)) {
                return i;
            }
        }
        throw new ArrayIndexOutOfBoundsException("Vertex doesn't exists");
    }

    /**
     * Metodo que devolve o peso da ligacao ao vertice recebido
     *
     * @param vertex vertice
     * @return peso ao ligacao ao vertice
     */
    public int getWeight(T vertex) {
        for (int i = 0; i < wAdjMatrix[getIndex(vertex)].length; i++) {
            if (wAdjMatrix[getIndex(vertex)][i] < Double.POSITIVE_INFINITY) {
                return (int) wAdjMatrix[i][getIndex(vertex)];
            }
        }
        return 0;
    }

    /**
     * Metodo que devolve o vertice com o indice recebido
     *
     * @param i indice do vertice
     * @return vertice
     */
    public T getVertex(int i) {
        if (i >= 0 && i < vertices.length)
            return vertices[i];
        else
            throw new ArrayIndexOutOfBoundsException("Vertex doesn't exists");
    }

    /**
     * Metodo que procura os vertices ligados ao vertice recebido
     *
     * @param vertex vertice
     * @return array de vertices ligados ao vertice recebido
     */
    public ArrayUnorderedList<T> getLigacoes(T vertex) {
        ArrayUnorderedList<T> ligacoes = new ArrayUnorderedList<>();

        for (int i = 0; i < wAdjMatrix[getIndex(vertex)].length; i++) {
            if (wAdjMatrix[getIndex(vertex)][i] < Double.POSITIVE_INFINITY) {
                ligacoes.addToFront(vertices[i]);
            }
        }
        return ligacoes;
    }
}
