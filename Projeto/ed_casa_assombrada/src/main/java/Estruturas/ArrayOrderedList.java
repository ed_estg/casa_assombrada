package Estruturas;

import Ficha_5.Ex_1.ArrayList;
import Interfaces.OrderedListADT;
import Models.Utilizador;

/**
 * Classe que permite criar uma lista ordenada das classificações/utilizadores
 *
 * @param <T> T
 */
public class ArrayOrderedList<T> extends ArrayList<T> implements OrderedListADT<T> {

    public ArrayOrderedList() {
        super();
    }

    /**
     * Metodo que adiciona o utilizador na devida posição, tendo em conta os pontos
     *
     * @param element objeto a colocar na lista
     */
    @Override
    public void add(T element) {
        //se o elemento não for comparavel
        if (count == list.length) // Se a lista estiver cheia
            expandCapacity();
        // Verificar o número de elementos existentes na lista
        // Se houver mais do que um elemento
        if (count == 0) { // Se a lista estiver vazia
            list[rear] = element;
        } else {//Se for menor que o primeiro elemento da lista>
            if (((Utilizador) element).getPontos() > ((Utilizador) list[front]).getPontos()) {
                //O novo elemento passa a ser o primeiro elemento da lista
                shiftArray(front, element);
            } //Se for maior que o último elemento da lista>
            else if (((Utilizador) element).getPontos() <= ((Utilizador) list[rear - 1]).getPontos()) {
                // O novo elemento passa a ser o último elemento da lista
                list[rear] = element;
            } //<Se o elemento tiver de ser inserido no meio da lista>
            else {
                int nElements = count; // Número de elementos
                for (int i = 0; i < nElements; i++) {
                    //Se o elemento for menor que algum elemento da lista>
                    if (((Utilizador) element).getPontos() > ((Utilizador) list[i]).getPontos()) {
                        // Fazer o shift do array a partir da posição encontrada
                        shiftArray(i, (T) element);
                        break;
                    }
                }
            }
        }
        rear++; // Atual posição livre
        count++; // Número de elementos decrementa
        modCount++; //Número de operações incrementa
    }

    /**
     * Método que permite expandir a capacidade da lista
     */
    private void expandCapacity() {
        int nElements = count; // Número de elementos
        int index = front; // Primeira posição
        // Novo lista de elementos
        T[] newArrayList = (T[]) new Object[list.length + 5];
        // Passar todos os elementos para o novo array
        for (int i = 0; i < nElements; i++) {
            newArrayList[i] = list[index];
            index = (index + 1) % list.length;
        }
        front = 0; // Primeira posição da lista
        rear = nElements; // Primeira posição livre da lista
        list = newArrayList; // Nova lista
    }

    /**
     * Metodo que coloca o elemento na posicao seguinte à posição recebida
     *
     * @param lastPositionToShift posicao anterior à posição onde pretende adicionar o elemento
     * @param element elemento a adicionar
     */
    private void shiftArray(int lastPositionToShift, T element) {
        if (count == list.length) // Se a lista estiver cheia
            expandCapacity();
        int firstFreePosition = rear; // Primeira posição livre da lista
        for (int i = firstFreePosition; i > lastPositionToShift; i--) {
            // A posição atual recebe o elemento da posição anterir
            list[i] = list[i - 1];
            // A posição anterior passa a estar vazia
            list[i - 1] = null;
        }
        // A posição vazia recebe o novo elemento
        list[lastPositionToShift] = element;
    }
}
